"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import argparse
import sched
import time
import traceback

import py_src.databases.DBCreator as dbc
import py_src.configuration.ConfigReader as conf
from py_src.databases.DBAccessor import DBAccessor
from py_src.datasources.CMEDownloader import CMEDownloader
from py_src.datasources.ElectronDownloader import ElectronDownloader
from py_src.datasources.FlareDownloader import FlareDownloader
from py_src.datasources.ProtonDownloader import ProtonDownloader
from py_src.processing.CMEProcessor import CMEProcessor
from py_src.processing.ElectronProcessor import ElectronProcessor
from py_src.processing.FlareProcessor import FlareProcessor
from py_src.processing.ProtonProcessor import ProtonProcessor


def do_start(config_dir, config_file, sc: sched):
    try:
        config_dir = os.path.abspath(config_dir)
        config = conf.ConfigReader(config_dir, config_file)
        logger = config.get_logger()
        if verify_db_exists(config):
            accessor = DBAccessor(config.get_pool(), config.get_logger())
            cme_dldr = CMEDownloader(config.get_logger())
            cme_processor = CMEProcessor(cme_dldr, accessor, config.get_logger())

            flare_dldr = FlareDownloader(config.get_logger())
            flare_processor = FlareProcessor(flare_dldr, accessor, config.get_logger())

            proton_dldr = ProtonDownloader(config.get_logger())
            proton_processor = ProtonProcessor(proton_dldr, accessor, config.get_logger())

            electron_dldr = ElectronDownloader(config.get_logger())
            electron_processor = ElectronProcessor(electron_dldr, accessor, config.get_logger())
            electron_processor.run()

            sc.enter(5, 1, do_cme_processing,
                     (cme_processor, config.get_logger(), config.get_cadence_cme() * 60 * 60, sc,))
            sc.enter(5, 1, do_flare_processing,
                     (flare_processor, config.get_logger(), config.get_cadence_flare() * 60 * 60, sc,))
            sc.enter(5, 1, do_proton_processing,
                     (proton_processor, config.get_logger(), config.get_cadence_proton() * 60 * 60, sc,))
            sc.enter(5, 1, do_electron_processing,
                     (electron_processor, config.get_logger(), config.get_cadence_electron() * 60 * 60, sc,))
        else:
            logger.error('Unable to create tables!')
            sc.enter(60, 1, do_start, (config_dir, config_file, sc,))
    except Exception as e:
        print('do_start Failed with: %s', str(e))
        print('do_start Traceback: %s', traceback.format_exc())
        sc.enter(60, 1, do_start, (config_dir, config_file, sc,))


def verify_db_exists(config: conf):
    creator = dbc.DBCreator(config.get_pool(), config.get_logger())
    if not creator.check_tables_exist():
        if creator.create_tables():
            return True
        else:
            return False
    else:
        return True


def do_cme_processing(cme_processor, logger, cadence_seconds: int, sc: sched):
    try:
        cme_processor.run()
    except Exception as e:
        logger.error('do_cme_processing Failed with: %s', str(e))
        logger.debug('do_cme_processing Traceback: %s', traceback.format_exc())
    sc.enter(cadence_seconds, 1, do_cme_processing, (cme_processor, logger, cadence_seconds, sc,))


def do_flare_processing(flare_processor, logger, cadence_seconds: int, sc: sched):
    try:
        flare_processor.run()
    except Exception as e:
        logger.error('do_flare_processing Failed with: %s', str(e))
        logger.debug('do_flare_processing Traceback: %s', traceback.format_exc())
    sc.enter(cadence_seconds, 1, do_flare_processing, (flare_processor, logger, cadence_seconds, sc,))


def do_proton_processing(proton_processor, logger, cadence_seconds: int, sc: sched):
    try:
        proton_processor.run()
    except Exception as e:
        logger.error('do_proton_processing Failed with: %s', str(e))
        logger.debug('do_proton_processing Traceback: %s', traceback.format_exc())
    sc.enter(cadence_seconds, 1, do_proton_processing, (proton_processor, logger, cadence_seconds, sc,))


def do_electron_processing(electron_processor, logger, cadence_seconds: int, sc: sched):
    try:
        electron_processor.run()
    except Exception as e:
        logger.error('do_electron_processing Failed with: %s', str(e))
        logger.debug('do_electron_processing Traceback: %s', traceback.format_exc())
    sc.enter(cadence_seconds, 1, do_electron_processing, (electron_processor, logger, cadence_seconds, sc,))


def run_main(config_dir, config_file):
    sc = sched.scheduler(time.time, time.sleep)
    sc.enter(5, 1, do_start, (config_dir, config_file, sc,))
    sc.run()


example_text = """example:

       python3 NRT-CME-Flare-Processor.py -d config_dir
       python3 NRT-CME-Flare-Processor.py -d config_dir -f configuration_file.ini
       python3 NRT-CME-Flare-Processor.py -f configuration_file.ini
       """
parser = argparse.ArgumentParser(prog='NRT-HARP-Data-Processor', description='NRT HARP Data Parameter Processing.',
                                 epilog=example_text,
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-d', '--directory', help='Location of the config file.', type=str,
                    default=os.path.join(os.getcwd(), 'config'))
parser.add_argument('-f', '--file', help='Name of the config file.', type=str, default='config.ini')
args = parser.parse_args()
run_main(args.directory, args.file)
