"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import traceback
from logging import Logger

from mysql.connector.pooling import MySQLConnectionPool


class DBCreator:
    CREATE_CME_CACTUS = '''CREATE TABLE `CME_CACTUS` (
      `Issue_Date` datetime NOT NULL,
      `CME` int NOT NULL,
      `t0` datetime DEFAULT NULL,
      `dt0` int DEFAULT NULL,
      `pa` int DEFAULT NULL,
      `da` int DEFAULT NULL,
      `v` int DEFAULT NULL,
      `dv` int DEFAULT NULL,
      `minv` int DEFAULT NULL,
      `maxv` int DEFAULT NULL,
      `halo` double DEFAULT NULL,
      PRIMARY KEY (`Issue_Date`, `CME`),
      INDEX `cme_issue_date_idx` (`Issue_Date`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_CME_CACTUS_EXISTS = '''SHOW TABLES LIKE 'CME_CACTUS';'''

    CREATE_INTEGRAL_ELECTRONS = '''CREATE TABLE `integral-electrons` (
      `time_tag` datetime NOT NULL,
      `satellite` int DEFAULT NULL,
      `>=2 MeV` double DEFAULT NULL,
      PRIMARY KEY (`time_tag`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_INTEGRAL_ELECTRONS_EXISTS = '''SHOW TABLES LIKE 'integral-electrons';'''

    CREATE_INTEGRAL_ELECTRONS_BACKUP = '''CREATE TABLE `integral-electrons-backup` (
      `time_tag` datetime NOT NULL,
      `satellite` int DEFAULT NULL,
      `>=2 MeV` double DEFAULT NULL,
      PRIMARY KEY (`time_tag`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_INTEGRAL_ELECTRONS_BACKUP_EXISTS = '''SHOW TABLES LIKE 'integral-electrons-backup';'''

    CREATE_INTEGRAL_PROTONS = '''CREATE TABLE `integral-protons` (
      `time_tag` datetime NOT NULL,
      `satellite` int DEFAULT NULL,
      `>=1 MeV` double DEFAULT NULL,
      `>=10 MeV` double DEFAULT NULL,
      `>=100 MeV` double DEFAULT NULL,
      `>=30 MeV` double DEFAULT NULL,
      `>=5 MeV` double DEFAULT NULL,
      `>=50 MeV` double DEFAULT NULL,
      `>=500 MeV` double DEFAULT NULL,
      `>=60 MeV` double DEFAULT NULL,
      PRIMARY KEY (`time_tag`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_INTEGRAL_PROTONS_EXISTS = '''SHOW TABLES LIKE 'integral-protons';'''

    CREATE_INTEGRAL_PROTONS_BACKUP = '''CREATE TABLE `integral-protons-backup` (
      `time_tag` datetime NOT NULL,
      `satellite` int DEFAULT NULL,
      `>=1 MeV` double DEFAULT NULL,
      `>=10 MeV` double DEFAULT NULL,
      `>=100 MeV` double DEFAULT NULL,
      `>=30 MeV` double DEFAULT NULL,
      `>=5 MeV` double DEFAULT NULL,
      `>=50 MeV` double DEFAULT NULL,
      `>=500 MeV` double DEFAULT NULL,
      `>=60 MeV` double DEFAULT NULL,
      PRIMARY KEY (`time_tag`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_INTEGRAL_PROTONS_BACKUP_EXISTS = '''SHOW TABLES LIKE 'integral-protons-backup';'''

    CREATE_XRAY_FLARES = '''
    CREATE TABLE `xray-flares` (
      `time_tag` timestamp NOT NULL,
      `begin_time` timestamp DEFAULT NULL,
      `begin_class` text,
      `max_time` timestamp DEFAULT NULL,
      `max_class` text,
      `max_ratio` double DEFAULT NULL,
      `max_ratio_time` timestamp DEFAULT NULL,
      `current_int_xrlong` double DEFAULT NULL,
      `end_time` timestamp DEFAULT NULL,
      `end_class` text,
      `satellite` double DEFAULT NULL,
      `NOAA_AR` text,
      `latitude` text,
      `longitude` text,
      PRIMARY KEY (`time_tag`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_XRAY_FLARES_EXISTS = '''SHOW TABLES LIKE 'xray-flares';'''

    CREATE_XRAY_FLARES_BACKUP = '''CREATE TABLE `xray-flares-backup` (
      `time_tag` timestamp NOT NULL,
      `begin_time` timestamp DEFAULT NULL,
      `begin_class` text,
      `max_time` timestamp DEFAULT NULL,
      `max_class` text,
      `max_ratio` double DEFAULT NULL,
      `max_ratio_time` timestamp DEFAULT NULL,
      `current_int_xrlong` double DEFAULT NULL,
      `end_time` timestamp DEFAULT NULL,
      `end_class` text,
      `satellite` double DEFAULT NULL,
      `NOAA_AR` text,
      `latitude` text,
      `longitude` text,
      PRIMARY KEY (`time_tag`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;'''

    CHECK_XRAY_FLARES_BACKUP_EXISTS = '''SHOW TABLES LIKE 'xray-flares-backup';'''

    def __init__(self, connection_pool: MySQLConnectionPool, logger: Logger):
        self._connection_pool = connection_pool
        self._logger = logger

    def check_tables_exist(self):
        """
          Checks if the CACTUS CME,  tables exist in the database.

          :return: False if any of the tables are missing. True if all are present.
        """
        answer = False
        con = self._connection_pool.get_connection()
        try:
            cur = con.cursor()
            cur.execute(DBCreator.CHECK_CME_CACTUS_EXISTS)
            row = cur.fetchone()
            if row is not None:
                cur.execute(DBCreator.CHECK_INTEGRAL_ELECTRONS_EXISTS)
                row = cur.fetchone()
                if row is not None:
                    cur.execute(DBCreator.CHECK_INTEGRAL_ELECTRONS_BACKUP_EXISTS)
                    row = cur.fetchone()
                    if row is not None:
                        cur.execute(DBCreator.CHECK_INTEGRAL_PROTONS_EXISTS)
                        row = cur.fetchone()
                        if row is not None:
                            cur.execute(DBCreator.CHECK_INTEGRAL_PROTONS_BACKUP_EXISTS)
                            row = cur.fetchone()
                            if row is not None:
                                cur.execute(DBCreator.CHECK_XRAY_FLARES_EXISTS)
                                row = cur.fetchone()
                                if row is not None:
                                    cur.execute(DBCreator.CHECK_XRAY_FLARES_BACKUP_EXISTS)
                                    row = cur.fetchone()
                                    if row is not None:
                                        answer = True
        except Exception as e:
            self._logger.error('DBCreator.check_tables_exist Failed with: %s', str(e))
            self._logger.debug('DBCreator.check_tables_exist Traceback: %s', traceback.format_exc())
        finally:
            con.close()
        return answer

    def create_tables(self):
        """
        Creates the tables for storing CME, Flare, Proton, and Electron data that is downloaded from various data
        sources.

        :return: True if completed, an exception if one occurs.
        """
        con = self._connection_pool.get_connection()
        try:
            cur = con.cursor()

            cur.execute(DBCreator.CHECK_CME_CACTUS_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_CME_CACTUS)
                con.commit()

            cur.execute(DBCreator.CHECK_INTEGRAL_ELECTRONS_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_INTEGRAL_ELECTRONS)
                con.commit()

            cur.execute(DBCreator.CHECK_INTEGRAL_ELECTRONS_BACKUP_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_INTEGRAL_ELECTRONS_BACKUP)
                con.commit()

            cur.execute(DBCreator.CHECK_INTEGRAL_PROTONS_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_INTEGRAL_PROTONS)
                con.commit()

            cur.execute(DBCreator.CHECK_INTEGRAL_PROTONS_BACKUP_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_INTEGRAL_PROTONS_BACKUP)
                con.commit()

            cur.execute(DBCreator.CHECK_XRAY_FLARES_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_XRAY_FLARES)
                con.commit()

            cur.execute(DBCreator.CHECK_XRAY_FLARES_BACKUP_EXISTS)
            row = cur.fetchone()
            if row is None:
                cur.execute(DBCreator.CREATE_XRAY_FLARES_BACKUP)
                con.commit()
        except Exception as e:
            self._logger.error('DBCreator.create_tables Failed with: %s', str(e))
            self._logger.debug('DBCreator.create_tables Traceback: %s', traceback.format_exc())
        finally:
            con.close()

        return True
