"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import traceback
import pandas as pd
from logging import Logger

from mysql.connector.pooling import MySQLConnectionPool
from pandas import DataFrame


class DBAccessor:
    CME_SELECT_BY_DATE_CME = """SELECT * FROM CME_CACTUS WHERE Issue_Date = %s and CME = %s """
    PRIMARY_FLARE_SELECT_BY_DATE = """SELECT * FROM `xray-flares` WHERE time_tag = %s """
    BACKUP_FLARE_SELECT_BY_DATE = """SELECT * FROM `xray-flares-backup` WHERE time_tag = %s """
    PRIMARY_PROTON_SELECT_BY_DATE = """SELECT * FROM `integral-protons` WHERE time_tag = %s """
    BACKUP_PROTON_SELECT_BY_DATE = """SELECT * FROM `integral-protons-backup` WHERE time_tag = %s """
    PRIMARY_ELECTRON_SELECT_BY_DATE = """SELECT * FROM `integral-electrons` WHERE time_tag = %s """
    BACKUP_ELECTRON_SELECT_BY_DATE = """SELECT * FROM `integral-electrons-backup` WHERE time_tag = %s """

    CME_INSERT = """INSERT INTO CME_CACTUS (Issue_Date, CME, t0, dt0, pa, da, v, dv, minv, maxv, halo) 
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
    PRIMARY_FLARE_INSERT = """INSERT INTO `xray-flares` (time_tag, begin_time, begin_class, max_time, max_class,
        max_ratio, max_ratio_time, current_int_xrlong, end_time, end_class, satellite, NOAA_AR, latitude, longitude) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
    BACKUP_FLARE_INSERT = """INSERT INTO `xray-flares-backup` (time_tag, begin_time, begin_class, max_time, max_class,
            max_ratio, max_ratio_time, current_int_xrlong, end_time, end_class, satellite, NOAA_AR, latitude, longitude) 
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """
    PRIMARY_PROTON_INSERT = """INSERT INTO `integral-protons` (time_tag, satellite, `>=1 MeV`, `>=10 MeV`, `>=100 MeV`,
            `>=30 MeV`, `>=5 MeV`, `>=50 MeV`, `>=500 MeV`, `>=60 MeV`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    BACKUP_PROTON_INSERT = """INSERT INTO `integral-protons-backup` (time_tag, satellite, `>=1 MeV`, `>=10 MeV`, 
            `>=100 MeV`, `>=30 MeV`, `>=5 MeV`, `>=50 MeV`, `>=500 MeV`, `>=60 MeV`) 
            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
    PRIMARY_ELECTRON_INSERT = """INSERT INTO `integral-electrons` (time_tag, satellite, `>=2 MeV`) VALUES (%s,%s,%s)"""
    BACKUP_ELECTRON_INSERT = """INSERT INTO `integral-electrons-backup` (time_tag, satellite, `>=2 MeV`) 
            VALUES (%s,%s,%s)"""

    def __init__(self, connection_pool: MySQLConnectionPool, logger: Logger):
        self._connection_pool = connection_pool
        self._logger = logger

    def insert_electron_report(self, report: DataFrame, primary: bool = True):
        """
        Method inserts a electron report from a dataframe object into the integral-electrons tables. If the report already
        exists the insert throws an exception, which is handled internally but logs the fact that it happened. It
        expects the dataframe to contain only one entry. If it doesn't, it only processes the first record.

        :param report: The Electron report DataFrame with the data to insert.
        :param primary: Indicates if we are interested in the primary source or the backup source
        :return: True if successful, false or throws an exception if not.
        """

        con = self._connection_pool.get_connection()
        try:

            if primary:
                query = DBAccessor.PRIMARY_ELECTRON_INSERT
            else:
                query = DBAccessor.BACKUP_ELECTRON_INSERT

            report = report.where(pd.notnull(report), None)

            time_tag = report.iloc[0]['time_tag'].strftime('%Y-%m-%d %H:%M:%S')
            satellite = int(report.iloc[0]['satellite'])
            ge2_MeV = float(report.iloc[0]['>=2 MeV']) if report.iloc[0]['>=2 MeV'] is not None else None

            cur = con.cursor()
            cur.execute(query, (time_tag, satellite, ge2_MeV))
            con.commit()
        except Exception as e:
            self._logger.error('DBAccessor.insert_electron_report Failed with: %s', str(e))
            self._logger.debug('DBAccessor.insert_electron_report Traceback: %s', traceback.format_exc())
            return False
        finally:
            con.close()
        return True

    def electron_report_exists(self, report: DataFrame, primary: bool = True):
        """
        Method checks if the integral electron info already exists in the integral-electrons tables.

        :param report:DataFame The report to check and see if it has been inserted already
        :param primary:bool Indicates if we are interested in the primary source or the backup source
        :return: True if info exists in table, else False or throws an Exception
        """
        answer = False
        con = self._connection_pool.get_connection()
        try:
            time_tag = report.iloc[0]['time_tag'].strftime('%Y-%m-%d %H:%M:%S')
            if primary:
                query = DBAccessor.PRIMARY_ELECTRON_SELECT_BY_DATE
            else:
                query = DBAccessor.BACKUP_ELECTRON_SELECT_BY_DATE

            cur = con.cursor()
            cur.execute(query, (time_tag,))
            row = cur.fetchone()
            if row is not None:
                answer = True
        except Exception as e:
            self._logger.error('DBAccessor.electron_report_exists Failed with: %s', str(e))
            self._logger.debug('DBAccessor.electron_report_exists Traceback: %s', traceback.format_exc())
        finally:
            con.close()

        return answer

    def insert_proton_report(self, report: DataFrame, primary: bool = True):
        """
        Method inserts a proton report from a dataframe object into the integral-protons tables. If the report already
        exists the insert throws an exception, which is handled internally but logs the fact that it happened. It
        expects the dataframe to contain only one entry. If it doesn't, it only processes the first record.

        :param report: The Proton report DataFrame with the data to insert.
        :param primary: Indicates if we are interested in the primary source or the backup source
        :return: True if successful, false or throws an exception if not.
        """

        con = self._connection_pool.get_connection()
        try:

            if primary:
                query = DBAccessor.PRIMARY_PROTON_INSERT
            else:
                query = DBAccessor.BACKUP_PROTON_INSERT

            report = report.where(pd.notnull(report), None)

            time_tag = report.iloc[0]['time_tag'].strftime('%Y-%m-%d %H:%M:%S')
            satellite = int(report.iloc[0]['satellite'])
            ge1_MeV = float(report.iloc[0]['>=1 MeV']) if report.iloc[0]['>=1 MeV'] is not None else None
            ge10_MeV = float(report.iloc[0]['>=10 MeV']) if report.iloc[0]['>=10 MeV'] is not None else None
            ge100_MeV = float(report.iloc[0]['>=100 MeV']) if report.iloc[0]['>=100 MeV'] is not None else None
            ge30_MeV = float(report.iloc[0]['>=30 MeV']) if report.iloc[0]['>=30 MeV'] is not None else None
            ge5_MeV = float(report.iloc[0]['>=5 MeV']) if report.iloc[0]['>=5 MeV'] is not None else None
            ge50_MeV = float(report.iloc[0]['>=50 MeV']) if report.iloc[0]['>=50 MeV'] is not None else None
            ge500_MeV = float(report.iloc[0]['>=500 MeV']) if report.iloc[0]['>=500 MeV'] is not None else None
            ge60_MeV = float(report.iloc[0]['>=60 MeV']) if report.iloc[0]['>=60 MeV'] is not None else None

            cur = con.cursor()
            cur.execute(query, (
                time_tag, satellite, ge1_MeV, ge10_MeV, ge100_MeV, ge30_MeV, ge5_MeV, ge50_MeV, ge500_MeV, ge60_MeV))
            con.commit()
        except Exception as e:
            self._logger.error('DBAccessor.insert_proton_report Failed with: %s', str(e))
            self._logger.debug('DBAccessor.insert_proton_report Traceback: %s', traceback.format_exc())
            return False
        finally:
            con.close()
        return True

    def proton_report_exists(self, report: DataFrame, primary: bool = True):
        """
        Method checks if the integral proton info already exists in the integral-protons tables.

        :param report:DataFame The report to check and see if it has been inserted already
        :param primary:bool Indicates if we are interested in the primary source or the backup source
        :return: True if info exists in table, else False or throws an Exception
        """
        answer = False
        con = self._connection_pool.get_connection()
        try:
            time_tag = report.iloc[0]['time_tag'].strftime('%Y-%m-%d %H:%M:%S')
            if primary:
                query = DBAccessor.PRIMARY_PROTON_SELECT_BY_DATE
            else:
                query = DBAccessor.BACKUP_PROTON_SELECT_BY_DATE

            cur = con.cursor()
            cur.execute(query, (time_tag,))
            row = cur.fetchone()
            if row is not None:
                answer = True
        except Exception as e:
            self._logger.error('DBAccessor.proton_report_exists Failed with: %s', str(e))
            self._logger.debug('DBAccessor.proton_report_exists Traceback: %s', traceback.format_exc())
        finally:
            con.close()

        return answer

    def flare_report_exists(self, report: DataFrame, primary: bool = True):
        """
        Method checks if the flare info already exists in the xray-flares table.

        :param report:DataFame The report to check and see if it has been inserted already
        :param primary:bool Indicates if we are interested in the primary source or the backup source
        :return: True if info exists in table, else False or throws an Exception
        """
        answer = False
        con = self._connection_pool.get_connection()
        try:
            time_tag = report.iloc[0]['time_tag'].strftime('%Y-%m-%d %H:%M:%S')
            if primary:
                query = DBAccessor.PRIMARY_FLARE_SELECT_BY_DATE
            else:
                query = DBAccessor.BACKUP_FLARE_SELECT_BY_DATE

            cur = con.cursor()
            cur.execute(query, (time_tag,))
            row = cur.fetchone()
            if row is not None:
                answer = True
        except Exception as e:
            self._logger.error('DBAccessor.flare_report_exists Failed with: %s', str(e))
            self._logger.debug('DBAccessor.flare_report_exists Traceback: %s', traceback.format_exc())
        finally:
            con.close()

        return answer

    def insert_flare_report(self, report: DataFrame, primary: bool = True):
        """
        Method inserts a flare report from a dataframe object into the xray-flares table. If the report already exists
        the insert throws an exception, which is handled internally but logs the fact that it happened. It expects the
        dataframe to contain only one entry. If it doesn't, it only processes the first record.

        :param report: The Flare report DataFrame with the data to insert.
        :param primary: Indicates if we are interested in the primary source or the backup source
        :return: True if successful, false or throws an exception if not.
        """

        con = self._connection_pool.get_connection()
        try:

            if primary:
                query = DBAccessor.PRIMARY_FLARE_INSERT
            else:
                query = DBAccessor.BACKUP_FLARE_INSERT

            report = report.where(pd.notnull(report), None)

            time_tag = report.iloc[0]['time_tag'].strftime('%Y-%m-%d %H:%M:%S')
            begin_time = report.iloc[0]['begin_time'].strftime('%Y-%m-%d %H:%M:%S') if report.iloc[0][
                                                                                           'begin_time'] is not (
                                                                                               None or pd.NaT) else None
            begin_class = report.iloc[0]['begin_class']
            max_time = report.iloc[0]['max_time'].strftime('%Y-%m-%d %H:%M:%S') if report.iloc[0][
                                                                                       'max_time'] is not (
                                                                                           None or pd.NaT) else None
            max_class = report.iloc[0]['max_class']
            max_ratio = float(report.iloc[0]['max_ratio']) if report.iloc[0]['max_ratio'] is not None else None
            max_ratio_time = report.iloc[0]['max_ratio_time'].strftime('%Y-%m-%d %H:%M:%S') if report.iloc[0][
                                                                                                   'max_ratio_time'] is not (
                                                                                                       None or pd.NaT) else None
            current_int_xrlong = float(report.iloc[0]['current_int_xrlong']) if report.iloc[0][
                                                                                    'current_int_xrlong'] is not None else None
            end_time = report.iloc[0]['end_time'].strftime('%Y-%m-%d %H:%M:%S') if report.iloc[0][
                                                                                       'end_time'] is not (
                                                                                           None or pd.NaT) else None
            end_class = report.iloc[0]['end_class']
            satellite = float(report.iloc[0]['satellite']) if report.iloc[0]['satellite'] is not None else None
            NOAA_AR = report.iloc[0]['NOAA_AR']
            latitude = report.iloc[0]['latitude']
            longitude = report.iloc[0]['longitude']

            cur = con.cursor()
            cur.execute(query, (time_tag, begin_time, begin_class, max_time, max_class,
                                max_ratio, max_ratio_time, current_int_xrlong, end_time,
                                end_class,
                                satellite, NOAA_AR, latitude, longitude))
            con.commit()
        except Exception as e:
            self._logger.error('DBAccessor.insert_flare_report Failed with: %s', str(e))
            self._logger.debug('DBAccessor.insert_flare_report Traceback: %s', traceback.format_exc())
            return False
        finally:
            con.close()
        return True

    def insert_cme_report(self, report: DataFrame):
        """
        Method inserts a CME report from a dataframe object into the CME_CACTUS table. If the report already exists
        the insert throws an exception, which is handled internally but logs the fact that it happened. It expects the
        dataframe to contain only one entry. If it doesn't, it only processes the first record.

        :param report: The CME report DataFrame with the data to insert.
        :return: True if successful, false or throws an exception if not.
        """

        con = self._connection_pool.get_connection()
        try:
            report = report.where(pd.notnull(report), None)
            issue_date = report.iloc[0]['Issue_Date'].strftime('%Y-%m-%d %H:%M:%S')
            cme_id = int(report.iloc[0]['CME'])
            t0 = report.iloc[0]['t0'].strftime('%Y-%m-%d %H:%M:%S') if report.iloc[0]['t0'] is not None else None
            dt0 = int(report.iloc[0]['dt0']) if report.iloc[0]['dt0'] is not None else None
            pa = int(report.iloc[0]['pa']) if report.iloc[0]['pa'] is not None else None
            da = int(report.iloc[0]['da']) if report.iloc[0]['da'] is not None else None
            v = int(report.iloc[0]['v']) if report.iloc[0]['v'] is not None else None
            dv = int(report.iloc[0]['dv']) if report.iloc[0]['dv'] is not None else None
            minv = int(report.iloc[0]['minv']) if report.iloc[0]['minv'] is not None else None
            maxv = int(report.iloc[0]['maxv']) if report.iloc[0]['maxv'] is not None else None
            halo = float(report.iloc[0]['halo']) if report.iloc[0]['halo'] is not None else None

            cur = con.cursor()
            cur.execute(DBAccessor.CME_INSERT, (issue_date, cme_id, t0, dt0, pa, da, v, dv, minv, maxv, halo))
            con.commit()
        except Exception as e:
            self._logger.error('DBAccessor.insert_cme_report Failed with: %s', str(e))
            self._logger.debug('DBAccessor.insert_cme_report Traceback: %s', traceback.format_exc())
            return False
        finally:
            con.close()
        return True

    def cme_report_exists(self, report: DataFrame):
        """
        Method checks if the cme information already exists in the table.

        :param report:DataFame The report to check and see if it has been inserted already
        :return: True if info exists in table, else False or throws an Exception
        """
        answer = False
        con = self._connection_pool.get_connection()
        try:
            issue_date = report.iloc[0]['Issue_Date'].strftime('%Y-%m-%d %H:%M:%S')
            cme_id = int(report.iloc[0]['CME'])

            cur = con.cursor()
            cur.execute(DBAccessor.CME_SELECT_BY_DATE_CME, (issue_date, cme_id,))
            row = cur.fetchone()
            if row is not None:
                answer = True
        except Exception as e:
            self._logger.error('DBAccessor.cme_report_exists Failed with: %s', str(e))
            self._logger.debug('DBAccessor.cme_report_exists Traceback: %s', traceback.format_exc())
        finally:
            con.close()

        return answer
