"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
from collections import defaultdict
from typing import Optional

import requests
import traceback
import pandas as pd

from logging import Logger
from pandas import DataFrame
from datetime import datetime


class ProtonDownloader:

    def __init__(self, logger: Logger):
        self._logger = logger
        self._report_url = 'https://services.swpc.noaa.gov/json/goes/primary/integral-protons-1-day.json'
        self._backup_report_url = 'https://services.swpc.noaa.gov/json/goes/primary/integral-protons-7-day.json'

    def get_reports(self) -> Optional[DataFrame]:
        try:
            resp = requests.get(url=self._report_url)
            if resp.status_code == 200:
                data = pd.read_json(resp.text)

                # Check if data field is empty
                if not data.empty:
                    # Extract and format data to fit in DataBase
                    data['time_tag'] = data['time_tag'].apply(
                        lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%SZ'))

                    # print(energy_level)
                    energy_levels = ['>=1 MeV', '>=10 MeV', '>=100 MeV', '>=30 MeV', '>=5 MeV', '>=50 MeV', '>=500 MeV',
                                     '>=60 MeV']
                    name_list = ['time_tag', 'satellite'] + energy_levels

                    entries_dict = defaultdict(dict)
                    for i in range(len(data.index)):
                        entries_dict[data['time_tag'][i]][data['energy'][i]] = data['flux'][i]
                        entries_dict[data['time_tag'][i]]['satellite'] = data['satellite'][i]

                    # Construct a dataframe of the data
                    df = pd.DataFrame(columns=name_list)
                    for k, v in entries_dict.items():
                        time_tag = k
                        row = [time_tag, v['satellite']]
                        for j in range(len(energy_levels)):
                            row.append(v[energy_levels[j]])
                        df.loc[len(df.index), :] = row

                    return df
        except Exception as e:
            self._logger.critical("ProtonDownloader.get_reports failed with error: %s", str(e))
            self._logger.debug("ProtonDownloader.get_reports failed traceback: %s", traceback.format_exc())

        return None

    def get_backup_reports(self) -> DataFrame:
        try:
            resp = requests.get(url=self._backup_report_url)
            if resp.status_code == 200:
                data = pd.read_json(resp.text)

                # Check if data field is empty
                if not data.empty:
                    # Extract and format data to fit in DataBase
                    data['time_tag'] = data['time_tag'].apply(
                        lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%SZ'))

                    energy_levels = ['>=1 MeV', '>=10 MeV', '>=100 MeV', '>=30 MeV', '>=5 MeV', '>=50 MeV', '>=500 MeV',
                                     '>=60 MeV']
                    name_list = ['time_tag', 'satellite'] + energy_levels

                    entries_dict = defaultdict(dict)
                    for i in range(len(data.index)):
                        entries_dict[data['time_tag'][i]][data['energy'][i]] = data['flux'][i]
                        entries_dict[data['time_tag'][i]]['satellite'] = data['satellite'][i]

                    # Construct a dataframe of the data
                    df = pd.DataFrame(columns=name_list)
                    for k, v in entries_dict.items():
                        time_tag = k
                        row = [time_tag, v['satellite']]
                        for j in range(len(energy_levels)):
                            row.append(v[energy_levels[j]])
                        df.loc[len(df.index), :] = row

                    return df
        except Exception as e:
            self._logger.critical("ProtonDownloader.get_backup_reports failed with error: %s", str(e))
            self._logger.debug("ProtonDownloader.get_backup_reports failed traceback: %s", traceback.format_exc())

        return None
