"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""

import requests
import traceback
import pandas as pd

from typing import List
from logging import Logger
from pandas import DataFrame
from datetime import datetime


class CMEDownloader:

    def __init__(self, logger: Logger):
        self._logger = logger
        self._report_url = 'http://www.sidc.oma.be/cactus/out/cmecat.txt'

    def get_reports(self) -> List[DataFrame]:
        try:
            resp = requests.get(url=self._report_url)
            if resp.status_code == 200:
                data = resp.text

                # Extract issue date and phrase date
                issue_date_str = data.split('\n:Product:')[0].split(':Issued: ')[1]
                issue_date = datetime.strptime(issue_date_str, '%a %b %d %H:%M:%S %Y')

                # Extract new data
                new_data = data.split("#")[-2].strip().split("\n")
                names = new_data.pop(0).split('|')

                # Check if data field is empty
                if len(new_data) > 0:
                    # Generate parameter list
                    name_list = ['Issue_Date']
                    for value in names:
                        value = value.strip()
                        if value == 'halo?':
                            value = value[:-1]
                        name_list.append(value)

                    # Format data to fit in DataBase
                    results = []
                    for i in range(len(new_data)):
                        new_data[i] = new_data[i].strip().split('|')
                        if len(new_data[i][-1]) == 0:
                            new_data[i][-1] = float('nan')

                        new_data[i][0] = int(new_data[i][0])
                        new_data[i][1] = pd.Timestamp(new_data[i][1])
                        new_data[i][2] = int(new_data[i][2])
                        new_data[i][3] = int(new_data[i][3])
                        new_data[i][4] = int(new_data[i][4])
                        new_data[i][5] = int(new_data[i][5])
                        new_data[i][6] = int(new_data[i][6])
                        new_data[i][7] = int(new_data[i][7])
                        new_data[i][8] = int(new_data[i][8])

                        data_list = [[issue_date] + new_data[i]]
                        df = pd.DataFrame(data_list, columns=name_list)
                        results.append(df)
                    return results

        except Exception as e:
            self._logger.critical("CMEDownloader.get_reports failed with error: %s", str(e))
            self._logger.debug("CMEDownloader.get_reports failed traceback: %s", traceback.format_exc())

        return None
