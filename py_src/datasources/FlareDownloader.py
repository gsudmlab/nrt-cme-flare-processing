"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""

import requests
import traceback
import pandas as pd
import numpy as np

from logging import Logger
from pandas import DataFrame

from datetime import datetime


class FlareDownloader:

    def __init__(self, logger: Logger):
        self._logger = logger
        self._report_url = 'https://services.swpc.noaa.gov/json/goes/primary/xray-flares-7-day.json'
        self._backup_report_url = 'https://services.swpc.noaa.gov/json/goes/primary/xray-flares-latest.json'

    def get_reports(self) -> DataFrame:
        try:
            resp = requests.get(url=self._report_url)
            if resp.status_code == 200:
                new_data = pd.read_json(resp.text)
                # Check if data field is empty
                if not new_data.empty:
                    new_data['time_tag'] = pd.to_datetime(new_data['time_tag'])
                    new_data = new_data.append(pd.DataFrame(columns=['NOAA_AR', 'latitude', 'longitude']), sort=False)
                    new_data['time_tag'] = new_data['time_tag'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['begin_time'] = new_data['begin_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['max_time'] = new_data['max_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['max_ratio_time'] = new_data['max_ratio_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['end_time'] = new_data['end_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                return new_data
        except Exception as e:
            self._logger.critical("FlareDownloader.get_reports failed with error: %s", str(e))
            self._logger.debug("FlareDownloader.get_reports failed traceback: %s", traceback.format_exc())

        return None

    def get_backup_reports(self):
        try:
            resp = requests.get(url=self._backup_report_url)
            if resp.status_code == 200:
                new_data = pd.read_json(resp.text)
                if not new_data.empty:
                    new_data = new_data.drop(columns=['current_class', 'current_ratio'])
                    new_data = new_data.append(pd.DataFrame(columns=['NOAA_AR', 'latitude', 'longitude']), sort=False)

                    new_data['time_tag'] = new_data['time_tag'].apply(
                        lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%SZ'))
                    new_data['begin_time'] = new_data['begin_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['end_time'] = new_data['end_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['max_time'] = new_data['max_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                    new_data['max_ratio'] = new_data['max_ratio'].apply(lambda x: np.nan if x == 'Unk' else x)
                    new_data['max_ratio_time'] = new_data['max_ratio_time'].apply(
                        lambda x: x.replace(tzinfo=None) if isinstance(x, datetime) else pd.NaT)
                return new_data
        except Exception as e:
            self._logger.critical("FlareDownloader.get_backup_reports failed with error: %s", str(e))
            self._logger.debug("FlareDownloader.get_backup_reports failed traceback: %s", traceback.format_exc())

        return None
