"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import logging
import logging.handlers
import configparser
from logging import Logger

from mysql.connector.pooling import MySQLConnectionPool


class ConfigReader:

    def __init__(self, conf_file_path: str, conf_file_name: str):
        config = configparser.ConfigParser()
        conf_file = conf_file_path + os.path.sep + conf_file_name
        config.read(conf_file)

        self._pool = self.__create_pool(config)
        self._logger = self.__create_logger(config)
        self._cadence_hours_cme = int(config['RUNTIME']['cadence_hours_cme'])
        self._cadence_hours_flare = int(config['RUNTIME']['cadence_hours_flare'])
        self._cadence_hours_proton = int(config['RUNTIME']['cadence_hours_proton'])
        self._cadence_hours_electron = int(config['RUNTIME']['cadence_hours_electron'])

    @staticmethod
    def __create_logger(config):
        log_dir = config['LOGGING']['log_path']
        log_file = config['LOGGING']['log_file']
        log_file_size = int(config['LOGGING']['log_file_size_bytes'])
        log_file_backups = int(config['LOGGING']['log_backups'])
        log_level = config['LOGGING']['level']

        if log_level == "DEBUG":
            log_level = logging.DEBUG
        elif log_level == "INFO":
            log_level = logging.INFO
        else:
            log_level = logging.CRITICAL

        formatter = logging.Formatter('%(name)s - %(asctime)s - %(levelname)s - %(message)s')
        logger = logging.getLogger("CME_FLARE_NRT")
        logger.setLevel(level=log_level)

        log_file = os.path.realpath(os.path.join(log_dir, log_file))
        handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=log_file_size, backupCount=log_file_backups)
        handler.setLevel(level=log_level)
        handler.setFormatter(formatter)

        logger.addHandler(handler)

        return logger

    @staticmethod
    def __create_pool(config):
        # Creates the dictionary vals, to create dbconfig
        vals = {"host": config['DATABASE']['host'], "user": config['DATABASE']['user'],
                "password": config['DATABASE']['password'], "database": config['DATABASE']['database'],
                "port": config['DATABASE']['port']}
        pool_size = int(config['DATABASE']['pool_size'])
        pool = MySQLConnectionPool(pool_size=pool_size, **vals)
        return pool

    def get_pool(self) -> MySQLConnectionPool:
        return self._pool

    def get_logger(self) -> Logger:
        return self._logger

    def get_cadence_cme(self) -> int:
        return self._cadence_hours_cme

    def get_cadence_flare(self) -> int:
        return self._cadence_hours_flare

    def get_cadence_proton(self) -> int:
        return self._cadence_hours_proton

    def get_cadence_electron(self) -> int:
        return self._cadence_hours_electron
