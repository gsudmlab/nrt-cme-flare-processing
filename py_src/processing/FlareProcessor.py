"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import traceback
from datetime import datetime
from logging import Logger

from py_src.databases.DBAccessor import DBAccessor
from py_src.datasources.FlareDownloader import FlareDownloader


class FlareProcessor:

    def __init__(self, downloader: FlareDownloader, accessor: DBAccessor, logger: Logger):
        self._downloader = downloader
        self._accessor = accessor
        self._logger = logger

    def run(self):
        time = datetime.now()
        self._logger.info("FlareProcessor initiated at %s", time)
        self.__run_primary()
        self.__run_secondary()
        time = datetime.now()
        self._logger.info("FlareProcessor completed at %s", time)

    def __run_primary(self):
        try:

            flare_reports = self._downloader.get_reports()
            if flare_reports is not None:
                for idx in range(len(flare_reports.index)):
                    df = flare_reports.iloc[[idx]]
                    if not self._accessor.flare_report_exists(df):
                        self._accessor.insert_flare_report(df)

        except Exception as e:
            self._logger.error('FlareProcessor.__run_primary Failed with: %s', str(e))
            self._logger.debug('FlareProcessor.__run_primary Traceback: %s', traceback.format_exc())

    def __run_secondary(self):
        try:

            flare_reports = self._downloader.get_backup_reports()
            if flare_reports is not None:
                for idx in range(len(flare_reports.index)):
                    df = flare_reports.iloc[[idx]]
                    if not self._accessor.flare_report_exists(df, False):
                        self._accessor.insert_flare_report(df, False)

        except Exception as e:
            self._logger.error('FlareProcessor.___run_secondary Failed with: %s', str(e))
            self._logger.debug('FlareProcessor.___run_secondary Traceback: %s', traceback.format_exc())
