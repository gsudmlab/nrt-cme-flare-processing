"""
 * NRT-CME-Flare-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2020 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import traceback
from datetime import datetime
from logging import Logger

from py_src.databases.DBAccessor import DBAccessor
from py_src.datasources.CMEDownloader import CMEDownloader


class CMEProcessor:

    def __init__(self, downloader: CMEDownloader, accessor: DBAccessor, logger: Logger):
        self._accessor = accessor
        self._downloader = downloader
        self._logger = logger

    def run(self):
        time = datetime.now()
        self._logger.info("CMEProcessor initiated at %s", time)
        try:

            cme_reports = self._downloader.get_reports()
            if cme_reports is not None:
                for df in cme_reports:
                    if not self._accessor.cme_report_exists(df):
                        self._accessor.insert_cme_report(df)

        except Exception as e:
            self._logger.error('CMEProcessor.run Failed with: %s', str(e))
            self._logger.debug('CMEProcessor.run Traceback: %s', traceback.format_exc())

        time = datetime.now()
        self._logger.info("CMEProcessor completed at %s", time)
